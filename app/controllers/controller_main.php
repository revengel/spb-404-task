<?php

class Controller_Main extends Controller {

	function __construct(){
		$this->model = new Model_Main();
		$this->view = new View();
	}

	function action_index(){
		$this->view->gen('main_view.php');
	}
}