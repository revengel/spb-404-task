<?php

class Controller_Student extends Controller {

	function __construct(){
		$this->model = new Model_Student();
		$this->view = new View();
	}

	function action_index($page = 1){
		$students = $this->model->get_students_table_data($page);
		$pages_count = $this->model->pages_count();
		$data = array(
			'students_list' => $students,
			'pages_count'	=> $pages_count,
			'page'			=> $page,
		);
		$this->view->gen_part('students_table_view.php', $data);
	}


	function action_top(){
		$top = $this->model->get_top_students_table_data();
		$data = array(
			'students' => !empty($top) ? $top : array(),
		);
		$this->view->gen_part('top_students_table_view.php', $data);
	}

	function action_form($id = null) {
		$data = null;
		if ($id && $this->model->item_exists((int)$id)) {
			$data = $this->model->get_row($id);
		}

		$groups_list = $this->model->Group->get_groups_list();
		$data = array(
			'groups_list' => $groups_list,
			'data'		  => $data ? $data : null,
		);
		$this->view->gen_part('students_form_view.php', $data);
	}


	function action_save( $id = null ){ 
		$id = (int) $id;
		$data = $_POST;

		if ($id && $this->model->item_exists($id)) {
			$res = $this->model->update_student($id, $data);
		} else {
			$res = $this->model->add_student($data);
		}

		if ($res) {
			$result = array( 'success' => true, );
		} else {
			$result = array( 'success' => false, );
		}

		$this->returnJSON($result);
	}

	function action_more($id) {
		$id = (int) $id;
		if ($this->model->item_exists($id)) {
			$marks = $this->model->Mark->get_marks($id);

			$avg_mark = $this->model->Mark->get_avg_mark($id);

			$char_text = $this->model->Char->get_char($id);
			// var_dump($char_text);

			$data = array(
				'marks' 	 => $marks,
				'avg_mark' => $avg_mark,
				'char_text' => $char_text ? $char_text : '',
				'student_id' => $id,
			);
			$this->view->gen_part('students_marks_view.php', $data);
		}
	}


	function action_delete($id) {
		$id = (int) $id;
		$result = false;
		if ($this->model->item_exists($id)) {
			$result = $this->model->delete_student($id);
		}
		$this->returnJSON(array('result' => $result));
	}


	function action_savemarks( $id ){
		$id = (int) $id;
		$marks = $_POST['marks'];
		$data = array();
		foreach ($marks as $subject_id => $subject) {
			foreach ($subject as $semestr_id => $mark) {
				if(!$mark) continue;
				$data[] = array(
					'student_id'	=> $id,
					'subject_id'	=> $subject_id,
					'semestr_id'	=> $semestr_id,
					'mark'			=> $mark,
				);
			}
		}
		$result = false;
		if ($id && $this->model->item_exists($id)) {
			$result = $this->model->Mark->save_marks($id, $data);
		}
		$this->returnJSON(array('result'=>$result));
	}


	function action_savechar($id) {
		$id = (int) $id;
		$result = false;
		$text = $_POST['char_text'];
		if ($id && $this->model->item_exists($id)) {
			$result = $this->model->Char->save_char($id, $text);
		}
		$this->returnJSON(array('result'=>$result));
	}

}