<?php

class Controller {
	
	var $model;
	var $view;

	function __construct() {
		$this->view = new View();
	}

	function action_index() {
		
	}

	function returnJSON(array $data) {
		header('Content-Type: application/json');
		echo json_encode(array('data' => $data));
		exit();
	}
}