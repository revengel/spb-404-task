<?php

class DbCon {
	public $db_connector;
	private static $instance;
 
	private function __construct() {
		global $db_host, $db_username, $db_password, $db_name;
		$this->db_connector = new PDO('mysql:host='.$db_host.';dbname='.$db_name, $db_username, $db_password);
	}
 
	public static function getInstance() {

		if (!isset(self::$instance)) {
			$object = __CLASS__;
			self::$instance = new $object;
		}
		return self::$instance;
	}

}