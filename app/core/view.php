<?php

class View {

	public $template_view;

	function gen ( $content_view, $template_view = 'template_view.php' , $data = null ){

		if ( is_array( $data ) ) {
			extract($data);
		}

		include "app/views/" . $template_view;
	}


	function gen_part($content_view, $data = null) {
		if ( is_array( $data ) ) {
			extract($data);
		}

		include "app/views/" . $content_view;
	}

}