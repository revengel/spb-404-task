<?php

require_once "model_group.php";
require_once "model_mark.php";
require_once "model_char.php";

class Model_Student extends Model {

	function __construct(){
		parent::__construct();
		$this->Group = new Model_Group();
		$this->Mark = new Model_Mark();
		$this->Char = new Model_Char();
	}

	
	function pages_count(){
		$db = $this->db;
		$sql = "SELECT COUNT(*)
				FROM students";
		$sql = $db->prepare($sql);
		$sql->execute();
		$res = $sql->fetchColumn();
		$res = ceil($res/50);
		return $res;
	}

	public function get_students_table_data( $page = 1) {
		$page = $page>0 ? $page : 1;
		$offset = 50*($page-1);
		$db = $this->db;
		$sql = "SELECT s.*, g.group_name
				FROM students s
				LEFT JOIN groups g ON s.group_id = g.id
				ORDER BY s.last_name, s.first_name
				LIMIT $offset, 50";
		$sql = $db->prepare($sql);
		$sql->execute();
		$res = $sql->fetchAll();

		return $res;
	}

	public function get_top_students_table_data() {
		$db = $this->db;
		$sql = "SELECT students.first_name, students.last_name, FORMAT(AVG(marks.mark),2) as avgmark
					FROM ( students, semestrs, subjects )
					INNER JOIN marks ON ( students.id = marks.student_id
										AND semestrs.id = marks.semestr_id
										AND subjects.id = marks.subject_id )
					GROUP BY students.id
					ORDER BY avgmark DESC, students.last_name, students.first_name
					LIMIT 20";
		$sql = $db->prepare($sql);
		$sql->execute();
		$res = $sql->fetchAll();

		return $res;
	}

	public function delete_student($id) {
		$db = $this->db;

		$db->beginTransaction();
		try {
			$sql = $db->prepare("DELETE FROM students WHERE id=$id");
			$sql->execute();
			$db->commit();
			return true;
		} catch (Exception $e) {
			$db->rollBack();
			return false;
		}

	}



	public function get_row($id) {
		$id = (int) $id;
		$db = $this->db;
		try {
			$db->beginTransaction();
			$sql = $db->prepare("SELECT s.* FROM students s WHERE id=:id LOCK IN SHARE MODE");
			$sql -> bindValue(':id', $id);
			$sql->execute();
			$res = $sql->fetch();
			return $res;
		} catch (Exception $e) {
			$db->rollBack();
			return false;
		}
	}

	public function add_student(array $columns){
		$db = $this->db;
		$data = array(
			'first_name' => $db->quote($columns['first_name']),
			'last_name' => $db->quote($columns['last_name']),
			'email' => $db->quote($columns['email']),
			'bdate' => $db->quote($columns['bdate']),
			'group_id' => $columns['group_id'],
			'ip' => $db->quote($_SERVER['REMOTE_ADDR']),
		);
		
		$cols = array_keys($data);

		$db->beginTransaction();
		try {
			$sql = "INSERT INTO students (". implode(',', $cols).") VALUES (". implode(',', $data).")";
			$sql = $db->prepare($sql);
			$sql->execute();
			$db->commit();
			return true;
		} catch (Exception $e) {
			$db->rollBack();
			echo $db->errorInfo();
			return false;
		}
	}


	function update_student($id, $columns) {
		$db = $this->db;

		$data = array(
			"first_name=".$db->quote($columns['first_name']),
			"last_name=".$db->quote($columns['last_name']),
			"email=".$db->quote($columns['email']),
			"bdate=".$db->quote($columns['bdate']),
			"group_id=".$columns['group_id'],
		);

		try {
			$db->beginTransaction();
			$sql = "UPDATE students SET ".implode(',', $data). " WHERE id=:id";
			$sql = $db->prepare($sql); 
			$sql -> bindValue(':id', $id);
			$sql->execute();
			$db->commit();
			return true;
		} catch (Exception $e) {
			$db->rollBack();
			echo $db->errorInfo();
			return false;
		}
	}



	function item_exists($id) {
		$db = $this->db;
		$sql = $db->prepare("SELECT COUNT(id) FROM students WHERE id = :id");
		$sql->bindValue(':id', $id);
		$sql->execute();
		$res = $sql->fetchColumn();
		$res = ((int)$res > 0) ? true: false;
		return $res;
	}


	



}