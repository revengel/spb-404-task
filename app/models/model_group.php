<?php

class Model_Group extends Model {


	function get_groups_list(){
		$db = $this->db;
		$sql = $db->prepare("SELECT * FROM groups ORDER BY group_name");
		$sql->execute();
		$res = array();
		while ($row = $sql->fetch()) {
			$res[$row['id']] = $row['group_name'];
		}
		return $res;
	}
}