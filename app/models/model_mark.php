<?php

class Model_Mark extends Model{

	function save_marks($student_id, $data){
		$id = (int) $student_id;
		$db = $this->db;
		try {
			$db->beginTransaction();
			
			$sql = $db->prepare("DELETE FROM marks WHERE student_id=$id");
			$sql->execute();

			$rows = array();
			foreach ($data as $row) {
				$rows[] = "(".implode(',', $row).")";
			}

			$sql = "INSERT INTO marks 
							(student_id,subject_id,semestr_id,mark)
						VALUES ".implode(',', $rows);
			$sql = $db->prepare($sql);
			$sql->execute();
			$db->commit();
			return true;
		} catch (Exception $e) {
			$db->rollBack();
			return false;
		}
	}

	function delete_marks($student_id) {
		$db = $this->db;
		try {
			$db->beginTransaction();
			$sql = $db->prepare("DELETE FROM marks WHERE student_id=$student_id");
			$sql->execute();
			$db->commit();
			return true;
		} catch (Exception $e) {
			$db->rollBack();
			return false;
		}

	}


	function get_marks($student_id) {
		$id = (int) $student_id;
		$db = $this->db;
		try{
			$sql = "SELECT subjects.id as subject_id, 
							subjects.subject_name, 
							semestrs.id as semestr_id, 
							semestrs.semestr_name, 
							marks.mark
					FROM (
						students, semestrs, subjects
					)
					LEFT JOIN marks ON ( students.id = marks.student_id
											AND semestrs.id = marks.semestr_id
											AND subjects.id = marks.subject_id )
					WHERE students.id = :id
					ORDER BY subjects.subject_name";

			$sql = $db->prepare($sql);
			$sql->bindValue(':id', $id);
			$sql->execute();
			$res = $sql->fetchAll();

			$data = array();

			foreach ($res as $item) {
				$data[$item['subject_id']][$item['semestr_id']][] = $item;
			}
			return $data;
		} catch (Exception $e) {
			echo $sql->errorInfo(); die;
			return false;
		}
	}



	function get_avg_mark($student_id) {
		$id = (int) $student_id;
		$db = $this->db;
		try{
			$sql = "SELECT FORMAT(AVG(marks.mark),2)
					FROM (
						students, semestrs, subjects
					)
					LEFT JOIN marks ON ( students.id = marks.student_id
											AND semestrs.id = marks.semestr_id
											AND subjects.id = marks.subject_id )
					WHERE students.id = :id";

			$sql = $db->prepare($sql);
			$sql->bindValue(':id', $id);
			$sql->execute();
			$res = $sql->fetchColumn();
			return $res;
		} catch (Exception $e) {
			echo $sql->errorInfo(); die;
			return false;
		}
	}

}