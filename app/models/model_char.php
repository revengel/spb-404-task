<?php

class Model_Char extends Model {

	function save_char($student_id, $text){
		$id = (int) $student_id;
		$db = $this->db;
		try {
			$db->beginTransaction();
			
			if ($this->char_exists($id)) {
				$sql = "UPDATE chars SET char_text='".$text."' WHERE student_id=".$id;
			} else {
				$sql = "INSERT INTO chars (student_id,char_text) VALUES (".$id.",'".$text."')";
			}
			$sql = $db->prepare($sql);
			$sql->execute();
			$db->commit();
			return true;
		} catch (Exception $e) {
			$db->rollBack();
			return false;
		}
	}

	function get_char($student_id) {
		$id = (int) $student_id;
		$db = $this->db;
		try{
			$sql = "SELECT char_text
					FROM chars
					WHERE student_id = :id";

			$sql = $db->prepare($sql);
			$sql->bindValue(':id', $id);
			$sql->execute();
			$res = $sql->fetchColumn();
			return $res;
		} catch (Exception $e) {
			echo $sql->errorInfo(); die;
			return false;
		}
	}

	function char_exists($student_id) {
		$id = (int) $student_id;
		$db = $this->db;
		try{
			$sql = "SELECT COUNT(*)
					FROM chars
					WHERE student_id = :id";

			$sql = $db->prepare($sql);
			$sql->bindValue(':id', $id);
			$sql->execute();
			$res = $sql->fetchColumn();
			return $res ? true : false;
		} catch (Exception $e) {
			echo $sql->errorInfo(); die;
			return false;
		}
	}
	
}