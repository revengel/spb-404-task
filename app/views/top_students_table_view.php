<table class="table">
	<thead>
		<th>Last Name</th>
		<th>First Name</th>
		<th>Average mark</th>
	</thead>
	<tbody>
		<?php if ($students) : ?>
			<?php foreach ($students as $student) : ?>
				<tr>
					<td><?php echo $student['last_name'];?></td>
					<td><?php echo $student['first_name'];?></td>
					<td><?php echo $student['avgmark'];?></td>
				</tr>
			<?php endforeach; ?>
		<?php else : ?>
			<tr>
				<td colspan="3">No data</td>
			</tr>
		<?php endif; ?>
	</tbody>
</table>