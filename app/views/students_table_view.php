<table class="table">
	<thead>
		<th>Last name</th>
		<th>First name</th>
		<th>Group</th>
		<th>Birth date</th>
		<th>Email</th>
		<th>IP address</th>
		<th>Registered</th>
		<th>Other</th>
	</thead>
	<tbody>
		<?php if (!empty($students_list)) : ?>
			<?php foreach ($students_list as $student) : ?>
				<tr>
					<td><?php echo $student['last_name']?></td>
					<td><?php echo $student['first_name']?></td>
					<td><?php echo $student['group_name']?></td>
					<td><?php echo $student['bdate']?></td>
					<td><?php echo $student['email']?></td>
					<td><?php echo $student['ip']?></td>
					<td><?php echo $student['registered']?></td>
					<td>
						<a href="/student/more/<?php echo $student['id']?>" class="showmore">
							<span class="glyphicon glyphicon-eye-open"></span>
						</a>

						<a href="#" class="hidden showless">
							<span class="glyphicon glyphicon-eye-close"></span>
						</a>

						<a href="/student/form/<?php echo $student['id']?>" class="edit_student_item">
							<span class="glyphicon glyphicon-pencil"></span>
						</a>

						<a href="/student/delete/<?php echo $student['id']?>" class="remove_student_item">
							<span class="glyphicon glyphicon-remove"></span>
						</a>

					</td>
				</tr>
			<?php endforeach;?>
		<?php else: ?>
			<tr>
				<td colspan="8">No data</td>
			</tr>
		<?php endif; ?>
	</tbody>
</table>

<ul class="pagination">
	<?php for ($i=1; $i<=$pages_count; $i++) : ?>
		<li class="<?php echo $page==$i?'active':''?>"><a href="/student/index/<?php echo $i?>"><?php echo $i?></a></li>
	<?php endfor; ?>
</ul>