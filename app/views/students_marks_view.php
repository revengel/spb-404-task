<tr class="student_more_content">
	<td colspan="8">
		<div class="col-md-8">

			<form action="/student/savemarks/<?php echo $student_id; ?>" method="post" onsubmit="return false;" class="student_marks_form">
				<table class="table">
					<?php if (!empty($marks)) : ?>
						<?php foreach ($marks as $subject_id => $subject) : ?>
							<?php $subject_name = reset($subject); ?>
							<tr>
								<td><?php echo $subject_name[0]['subject_name']?></td>
								<?php foreach ($subject as $semestr_id => $semestr) : ?>
									<td>
										<input type="num" value="<?php echo $semestr[0]['mark']?>" name="marks[<?php echo $subject_id;?>][<?php echo $semestr_id; ?>]">
									</td>
								<?php endforeach; ?>
							</tr>
						<?php endforeach; ?>
					<?php else : ?>
						<tr>
							<td>No data</td>
						</tr>
					<?php endif; ?>
				</table>
				<?php if ($avg_mark) : ?>
					<h1><?php echo $avg_mark?></h1>
				<?php endif;?>
				<input type="submit" name="submit" value="Save" class="btn btn-default"/>
			</form>	

		</div>

		<div class="col-md-4">
			<form action="/student/savechar/<?php echo $student_id; ?>" method="post" onsubmit="return false" class="char_text_form">
				<textarea name="char_text" class="char_text" rows="15"><?php echo $char_text; ?></textarea>
				<input type="submit" name="submit" value="Save" class="btn btn-default"/>
			</form>
		</div>
	</td>
</tr>