<div class="row">
	<form class="form-horizontal save_student_form" method="post" action="/student/save<?php echo ($data && $data['id'])?'/'.$data['id']:''?>" onsubmit="return false;">
		<fieldset>
			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="last_name">Last name</label>
				<div class="col-md-4">
					<input id="last_name" name="last_name" type="text" placeholder="" class="form-control input-md" required="" value="<?php echo $data ? $data['last_name']:''?>">
				</div>
			</div>
			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="first_name">First name</label>
				<div class="col-md-4">
					<input id="first_name" name="first_name" type="text" placeholder="" class="form-control input-md" required value="<?php echo $data ? $data['first_name']:''?>">
				</div>
			</div>
			<!-- Select Basic -->
			<div class="form-group">
				<label class="col-md-4 control-label" for="group_id">Group</label>
				<div class="col-md-4">
					<select id="group_id" name="group_id" class="form-control" required>
						<option value="">Please select group</option>
						<?php foreach ($groups_list as $id => $group_name) : ?>
							<?php $selected = ($data && $data['group_id']==$id) ? 'selected' : '';?>
							<option <?php echo $selected?> value="<?php echo $id ?>"><?php echo $group_name ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="bdate">Date of birth</label>
				<div class="col-md-4">
					<input id="bdate" name="bdate" type="date" placeholder="" class="form-control input-md" required="" value="<?php echo $data ? $data['bdate']:''?>">
				</div>
			</div>
			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="email">Email</label>
				<div class="col-md-4">
					<input id="email" name="email" type="email" placeholder="" class="form-control input-md" required value="<?php echo $data ? $data['email']:''?>">
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<input type="submit" class="btn btn-default" value="Save" />
					<input type="reset" class="btn btn-default reset_add_student_form" value="Cancel" />
				</div>

				
			</div>

			
		</fieldset>
	</form>
</div>