SELECT students.first_name, students.last_name, FORMAT(AVG(marks.mark),2) as avgmark
FROM students
INNER JOIN marks ON students.id = marks.student_id
WHERE students.last_name LIKE '%orld%'
GROUP BY students.id
HAVING avgmark > 4 AND avgmark < 10
ORDER BY avgmark DESC, students.last_name, students.first_name;



SELECT s.last_name, s.first_name, s.ip,c.char_text
FROM students s
LEFT JOIN chars c ON s.id = c.student_id
WHERE ip IN
(
	SELECT s.ip
	FROM students s
	LEFT JOIN chars c ON s.id=c.student_id
	GROUP BY s.ip
	HAVING COUNT(s.ip) > 1 AND COUNT(c.student_id)>0
)