$(function(){
	var app = {

		top_container: $('.top_container'),
		after_item_container: $('tr.student_more_content'),

		init: function(){
			this.addEventsListeners();

			this.loadStudentsTable();

			this.loadTopStudentsTable();
		},

		addEventsListeners: function(){
			$('.reload_students_list').on('click', this.loadStudentsTable);
			$('.create_new_student_button').on('click', {object: this}, this.createNewStudentLink);

			$(document).on('submit', '.save_student_form', {object: this}, this.saveStudentFormSubmit);

			$(document).on('submit', '.student_marks_form', {object: this}, this.saveStudentMarksFormSubmit);
			
			$(document).on('submit', '.char_text_form', {object: this}, this.saveStudentCharTextFormSubmit);

			$(document).on('click', '.reset_add_student_form', this.resetContainers);
			$(document).on('click', '.showmore', this.studentShowMoreEvent);
			$(document).on('click', '.showless', this.studentShowLessEvent);

			$(document).on('click', 'a.remove_student_item', this.onClickRemoveStudentItem);
			$(document).on('click', 'a.edit_student_item', this.onClickEditStudentItem);

			$(document).on('click', 'ul.pagination>li>a', this.onClickPagination );
		},

		resetContainers: function(){
			app.top_container.empty();
			app.deleteItemContainers();
		},

		reloadWithAjax: function(container, config) {
			$.ajax( config )
				.done(function( html ) {
					container.html( html );
			});
		},

		createItemContainer: function(data){
			return $('<tr class="student_more_content"><td colspan=8>' + data + '</td></td>');
		},

		deleteItemContainers: function(){
			$('.showless:visible').click();
			$('showless').addClass('hidden');
			$('showmore').removeClass('hidden');
			$('.student_more_content').remove();
		},

		loadStudentsTable: function(page){
			var page = page || 1,
				container = $('body .studets_main_table'),
				config = {
					url: "/student/index/" + page,
					cache: false
				};
			app.reloadWithAjax(container, config);
		},

		loadTopStudentsTable: function(){
			var container = $('body .top_students_table'),
				config = {
					url: "/student/top",
					cache: false
				};
			app.reloadWithAjax(container, config);
		},

		onClickPagination: function(e) {
			e.preventDefault();
			var page = $(this).attr('href').split('/').pop();
			app.loadStudentsTable( page );
		},

		createNewStudentLink: function(e) {
			var object = e.data.object,
				$this = $(this),
				config = {
					url: "/student/form",
					cache: false
				};
			e.preventDefault();
			object.reloadWithAjax(object.top_container, config);
		},



		saveStudentFormSubmit: function(e){
			var object = e.data.object,
				$this = $(this),
				url = $this.attr('action'),
				data = $this.serialize(),
				config = {
					url: url,
					type: 'post',
					cache:false,
					data: data,
					dataType: 'json'
				};

			$.ajax( config ).done(function(res){
				var success = res.data.success;
				if (success) {
					alert('Item has been saved successfully');
					object.top_container.empty();
					object.loadStudentsTable();
				}
			});
		},



		saveStudentMarksFormSubmit: function(e){
			var $this = $(this),
				url = $this.attr('action'),
				method = $this.attr('method'),
				data = $this.serialize(),
				config = {
					url: url,
					type: method,
					cache:false,
					data: data,
					dataType: 'json'
				};

			$.ajax( config ).done(function(res){
				var success = res.data.result;
				if (success) {
					$this.closest('.student_more_content').prev('tr').find('.showmore').click();
					app.loadTopStudentsTable();
				}
			});
		},


		saveStudentCharTextFormSubmit: function(e) {
			var $this = $(this),
				url = $this.attr('action'),
				method = $this.attr('method'),
				data = $this.serialize(),
				config = {
					url: url,
					type: method,
					cache:false,
					data: data,
					dataType: 'json'
				};

			$.ajax( config ).done(function(res){
				var success = res.data.result;
			});
		},




		studentShowMoreEvent: function(e) {
			var $this = $(this),
				togglers = $this.closest('td').find('.showmore, .showless'),
				url = $this.attr('href'),
				row = $this.closest('tr');
			e.preventDefault();

			$.ajax({
				url: url,
				cache: false,
				type: 'post'
			}).done(function( data ){
				app.deleteItemContainers();
				togglers.toggleClass('hidden');
				row.after(data);
			});
		},

		studentShowLessEvent: function(e) {
			var $this = $(this),
				togglers = $this.closest('td').find('.showmore, .showless');
			e.preventDefault();
			togglers.toggleClass('hidden');
			app.deleteItemContainers();
		},

		onClickRemoveStudentItem: function(e){
			e.preventDefault();
			var $this = $(this),
				url = $this.attr('href');
			if (confirm ('Are you sure ?')) {
				$.ajax({
					url: url,
					cache: false,
					type: 'post',
					dataType: 'json'
				}).done(function(res){
					var result = res.data.result;
					if (result) {
						alert('The item was deleted.');
						app.loadStudentsTable();
					} else {
						alert('Fatal error. Can\'t delete item.');
					}
				});
			}
		},


		onClickEditStudentItem: function(e){
			e.preventDefault();
			
			var $this = $(this),
				url = $this.attr('href'),
				row = $this.closest('tr'),
				config = {
					url: url,
					cache: false
				};

				$.ajax(config)
				.done(function( data ) {
					app.deleteItemContainers();
					row.after( app.createItemContainer(data) );
				});
				
		}


	};

	app.init();

});